package com.example.amanagement;

import android.content.Context;
import android.widget.TextView;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.robotium.solo.Solo;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<Login> activityTestRule = new ActivityTestRule<>(Login.class);
//    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    private Context context;
    private Solo solo;

    @Before
    public void setUp() throws Exception {
        //setUp() is run before a test case is started.'8/
        //This is where the solo object is created.
        solo = new Solo(InstrumentationRegistry.getInstrumentation(), activityTestRule.getActivity());
        context = InstrumentationRegistry.getInstrumentation().getTargetContext().getApplicationContext();
    }

    @After
    public void tearDown() throws Exception {
        //tearDown() is run after a test case has finished.
        //finishOpenedActivities() will finish all the activities that have been opened during the test execution.
        solo.finishOpenedActivities();
    }

    @Test
    public void testLogin() throws Exception{
        solo.enterText(0,"B0303");
        solo.enterText(1,"123456");
        solo.clickOnButton(0);
        solo.searchText("Đăng nhập thành công");
        solo.waitForActivity(MainActivity.class, 3000);
        solo.assertCurrentActivity("Phai toi duoc Main Activity", MainActivity.class);
        solo.goBack();
    }

    @Test
    public void loginFail() throws Exception{
        solo.enterText(0, "");
        solo.clickOnButton(0);
        TextView toast = (TextView) solo.getView(android.R.id.message);

        if (toast.equals("Vui lòng nhập tài khoản")){
            solo.enterText(0, "OK");
        }
        else{
            assertEquals("toast is not showing", "Vui lòng nhập tài khoản", toast.getText().toString());
        }
    }

}