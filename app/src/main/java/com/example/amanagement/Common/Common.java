package com.example.amanagement.Common;

import com.example.amanagement.Model.NewsModel;
import com.example.amanagement.Model.NotiModel;
import com.example.amanagement.Model.UserModel;

public class Common {

    public static final String USER_REFERENCES = "User";
    public static final String NEWS_REFERENCE = "news" ;
    public static final String NOTI_REFERENCE = "Notification" ;
    public static UserModel currentUser;

    public static NotiModel NotiClick;
    public static NewsModel NewsClick;

    public static UserModel ProfileClick;
}
