package com.example.amanagement;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.amanagement.Common.Common;
import com.example.amanagement.Model.UserModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class Login extends AppCompatActivity {

    private static final String TAG = "hi";
    // Khai bao bien
    Button btnLogin;
    EditText account, password;

    Boolean validate = false;

    // Khai bao data
    DatabaseReference userRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Lien ket voi xml
        linkView();

        // Ket noi database
        userRef = FirebaseDatabase.getInstance("https://amamagement-478ec-default-rtdb.firebaseio.com/").getReference(Common.USER_REFERENCES);

        // Xu ly su kien dang nhap
        addEvent();
        // ID - Pass: B0303 - 123456

    }

    private void linkView() {
        btnLogin = findViewById(R.id.btnLogin);
        account = findViewById(R.id.etID);
        password = findViewById(R.id.etPass);
    }

    private void addEvent() {

        // Su kien nut login
        btnLogin.setOnClickListener(v -> {
            // Goi ham check
            Log.d(TAG, ""+ checkInput());
            if (checkInput()) {
                //check id
                // Kiem tra tai khoan va dang nhap
                userRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //kiem tra tai khoan trong database

                        for (DataSnapshot blockSnapshot : dataSnapshot.getChildren()) {
                            if (blockSnapshot.child(account.getText().toString()).exists()) {
                                validate = true;
                                UserModel user = blockSnapshot.child(account.getText().toString()).getValue(UserModel.class);
                                if (user.getPassword().equals(password.getText().toString())) {
                                    Toast.makeText(Login.this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();
                                    gotohomeactivity(user);
                                } else {
                                    Toast.makeText(Login.this, "Sai mật khẩu", Toast.LENGTH_SHORT).show();
                                }
                            } else if (!validate) {
                                Toast.makeText(Login.this, "Tài khoản không tồn tại", Toast.LENGTH_SHORT).show();
                            }
                        }
//                    if (dataSnapshot.child(account.getText().toString()).exists()) {
//                        //lay thong tin nguoi dung
//                        UserModel user = dataSnapshot.child(account.getText().toString()).getValue(UserModel.class);
//                        if (user.getPassword().equals(password.getText().toString())) {
//                            userRef.child(user.getAccount()).setValue(user)
//                                    .addOnCompleteListener(task -> {
//                                        if (task.isSuccessful()) {
//                                            Toast.makeText(Login.this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();
//                                            UserModel userModel = dataSnapshot.getValue(UserModel.class);
//                                            gotohomeactivity(userModel);
//                                        }
//                                    });
//                        } else {
//                            Toast.makeText(Login.this, "Sai mật khẩu", Toast.LENGTH_SHORT).show();
//                        }
//                    } else {
//                        Toast.makeText(Login.this, "Tài khoản không tồn tại", Toast.LENGTH_SHORT).show();
//                    }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(Login.this, "DatabaseError", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            else if (!checkInput()){
                Toast.makeText(this, "Login fail", Toast.LENGTH_SHORT).show();
            }
        });

    }

    // Chuyen toi Activity Home
    private void gotohomeactivity(UserModel user) {
        Common.currentUser = user;
        startActivity(new Intent(Login.this, IntroActivity.class));
    }

    // Kiem tra du lieu nhap vao Edit text
    private boolean checkInput() {
        if (account.getText().toString().length() == 0 || password.getText().toString().length() == 0) {
            Toast.makeText(this, "Khong duoc de trong", Toast.LENGTH_SHORT).show();
            return false;
        }
        else{
            return true;
        }
    }
}

