package com.example.amanagement.callback;

import android.view.View;

public interface CallBackClickNoti {
    void OnItemClickListener(View view, int pos);
}
