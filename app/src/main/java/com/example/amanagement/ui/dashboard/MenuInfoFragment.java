package com.example.amanagement.ui.dashboard;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.amanagement.Common.Common;
import com.example.amanagement.R;



public class MenuInfoFragment extends Fragment {

    TextView tvname, tvapartment, tvblock, tvphone, tvmail, tvidcontract;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_menu_info, container, false);

        tvname = root.findViewById(R.id.tvname);
        tvapartment = root.findViewById(R.id.tvapartment);
        tvblock = root.findViewById(R.id.tvblock);
        tvphone = root.findViewById(R.id.tvphone);
        tvmail = root.findViewById(R.id.tvmail);
        tvidcontract = root.findViewById(R.id.tvidcontract);

        tvname.setText(Common.currentUser.getOwner().toString());
        tvapartment.setText(Common.currentUser.getApartment().toString());
        tvblock.setText(Common.currentUser.getBlock().toString());
        tvphone.setText(Common.currentUser.getPhone().toString());
        tvmail.setText(Common.currentUser.getEmail().toString());
        tvidcontract.setText(Common.currentUser.getIdbusiness().toString());

        return root;
    }

}