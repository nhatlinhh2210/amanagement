package com.example.amanagement.ui.menu;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProvider;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.amanagement.Common.Common;
import com.example.amanagement.Login;
import com.example.amanagement.Model.UserModel;
import com.example.amanagement.R;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MenuFragment extends Fragment {

    private MenuViewModel mViewModel;
    NavController navController;
    int a = 0;

    RelativeLayout rlutility, rlinfoapp, rlsetting,rlinfo,rllogout;
    AlertDialog alertDialog;

    TextView tvidinfo;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.menu_fragment, container, false);
        tvidinfo = root.findViewById(R.id.tvidinfo);
        tvidinfo.setText(Common.currentUser.getOwner().toString());
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MenuViewModel.class);
        // TODO: Use the ViewModel

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        rllogout = view.findViewById(R.id.rllogout);

        rlinfo = view.findViewById(R.id.rlinfo);
        rlutility = view.findViewById(R.id.rlutility);
        rlinfoapp = view.findViewById(R.id.rlinfoapp);
        rlsetting = view.findViewById(R.id.rlsetting);
        rllogout = view.findViewById(R.id.rllogout);

        rlinfo.setOnClickListener(v ->
                navController.navigate(R.id.action_navigation_dashboard_to_menu_profile_fragment));

        rlutility.setOnClickListener(v -> {
            navController.navigate(R.id.action_navigation_dashboard_to_utility_fragment);
        });
        rlinfoapp.setOnClickListener(v -> {
            navController.navigate(R.id.action_navigation_dashboard_to_info_app_fragment);
        });
        rlsetting.setOnClickListener(v ->
                navController.navigate(R.id.action_navigation_dashboard_to_setting_fragment));
        rllogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOut();
            }
        });
    }


    private void signOut() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Đăng Xuất")
                .setMessage("Bạn có thực sự muốn đăng xuất ?")
                .setNegativeButton("HỦY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).setPositiveButton("ĐỒNG Ý", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Common.currentUser = null;
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(getActivity(), Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}