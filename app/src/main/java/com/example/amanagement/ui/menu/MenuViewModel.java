package com.example.amanagement.ui.menu;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.amanagement.Common.Common;
import com.example.amanagement.Model.UserModel;

public class MenuViewModel extends ViewModel {
    private MutableLiveData<UserModel> mutableLiveDataNoti;
    public MenuViewModel(){
    }

    public MutableLiveData<UserModel> getMutableLiveDataNoti(){
        if (mutableLiveDataNoti == null){
            mutableLiveDataNoti = new MutableLiveData<>();
        }
        mutableLiveDataNoti.setValue(Common.currentUser);
        return mutableLiveDataNoti;
    }
}